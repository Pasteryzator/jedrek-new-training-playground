package main.java.pl.sda;

public class CalculateCircumference {

    public static float Circumference (float diameter) {
        float resultsimple = (float) (diameter * 3.14);
        System.out.println("Obwód wynosi" + " " + resultsimple);
        return resultsimple;
    }

    public static float CircumferencePrecise (float diameter) {
        float resultprecise = (float) (diameter * Math.PI);
        System.out.println("Obwód dokładny wynosi "+ " " + resultprecise);
        return resultprecise;
    }





}
